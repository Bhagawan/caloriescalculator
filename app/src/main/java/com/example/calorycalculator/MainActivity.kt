package com.example.calorycalculator

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Rect
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.PopupWindow
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.lifecycleScope
import com.example.calorycalculator.databinding.ActivityMainBinding
import com.example.calorycalculator.databinding.PopupAddConsumptionBinding
import com.example.calorycalculator.util.SavedPrefs
import com.example.calorycalculator.util.adapters.HistoryAdapter
import com.example.calorycalculator.viewmodel.AddConsumptionViewModel
import com.example.calorycalculator.viewmodel.MainViewModel
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var target: Target

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        binding.viewModel = MainViewModel()
        binding.lifecycleOwner = this
        binding.recyclerHistory.adapter = HistoryAdapter()
        CoroutineScope(Dispatchers.Main).launch{ binding.viewModel?.commandFlow?.collect {
            when(it) {
                MainViewModel.COMMAND_EDIT_PROFILE -> editProfile()
                MainViewModel.COMMAND_ADD_CONSUMPTION -> addConsumption()
                MainViewModel.COMMAND_CLEAR -> clearHistory()
            }
        } }
        setContentView(binding.root)
        setBackground()
    }

    override fun onResume() {
        binding.viewModel?.updateGoal()
        super.onResume()
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun addConsumption() {
        val popupBinding: PopupAddConsumptionBinding = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.popup_add_consumption, null, false)

        val width = LinearLayout.LayoutParams.MATCH_PARENT
        val height = LinearLayout.LayoutParams.MATCH_PARENT

        val popupWindow = PopupWindow(popupBinding.root, width, height, true)

        val popupViewModel = AddConsumptionViewModel()
        lifecycleScope.launch {
            popupViewModel.buttonListener.collect {
                if(it != null) {
                    SavedPrefs.addConsumption(this@MainActivity, it)
                    binding.viewModel?.updateHistory()
                    val amount = binding.recyclerHistory.adapter?.itemCount ?: 0
                    if (amount > 0) binding.recyclerHistory.scrollToPosition(amount)
                }
                popupWindow.dismiss()
            }
        }

        popupWindow.setBackgroundDrawable( ColorDrawable(Color.TRANSPARENT))
        popupWindow.isTouchable = true
        popupWindow.isOutsideTouchable = true
        popupBinding.root.findFocus()

        popupWindow.setTouchInterceptor{ _, event ->
            if (event?.action == MotionEvent.ACTION_DOWN) {
                val v = popupBinding.root.findFocus()
                if (v is EditText) {
                    val outRect = Rect()
                    v.getGlobalVisibleRect(outRect)
                    if (!outRect.contains(event.rawX.toInt(), event.rawY.toInt())) {
                        v.clearFocus()
                        val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0)
                    }
                }
            }
            false
        }

        popupBinding.setVariable(BR.viewModel, popupViewModel)

        popupWindow.animationStyle = R.style.PopupAnimation
        popupWindow.showAtLocation(binding.root, Gravity.CENTER, 0,0)

        val p = popupWindow.contentView.rootView.layoutParams as WindowManager.LayoutParams
        p.flags = p.flags or WindowManager.LayoutParams.FLAG_DIM_BEHIND or WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        p.dimAmount = 0.26f

        val wm: WindowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager
        wm.updateViewLayout(popupWindow.contentView.rootView, p)
    }

    private fun editProfile() {
        startActivity(Intent(this, ProfileActivity::class.java))
    }

    private fun clearHistory() {
        SavedPrefs.clearHistory(this)
        binding.viewModel?.updateHistory()
    }

    private fun showError(s: String) = Toast.makeText(this, s, Toast.LENGTH_SHORT).show()

    private fun setBackground() {
        target = object : Target {
            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                bitmap?.let { binding.layoutMain.background = BitmapDrawable(resources, it) }
            }

            override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) { }

            override fun onPrepareLoad(placeHolderDrawable: Drawable?) { }

        }
        Picasso.get().load("http://195.201.125.8/CaloriesCalculator/background.png").into(target)
    }
}