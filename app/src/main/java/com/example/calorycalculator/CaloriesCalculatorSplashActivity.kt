package com.example.calorycalculator

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Rect
import android.os.Bundle
import android.telephony.TelephonyManager
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.webkit.WebChromeClient
import android.widget.EditText
import android.widget.FrameLayout
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import com.example.calorycalculator.databinding.ActivityCaloriesCalculatorSplashBinding
import com.example.calorycalculator.util.SavedPrefs
import com.example.calorycalculator.viewmodel.CaloriesCalculatorSplashViewModel

@SuppressLint("CustomSplashScreen")
class CaloriesCalculatorSplashActivity : AppCompatActivity() {
    private lateinit var binding: ActivityCaloriesCalculatorSplashBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityCaloriesCalculatorSplashBinding.inflate(layoutInflater)
        val viewModel = CaloriesCalculatorSplashViewModel((getSystemService(TELEPHONY_SERVICE) as TelephonyManager).simCountryIso, SavedPrefs.getId(this))
        binding.viewmodel = viewModel
        viewModel.switchToMain.observe(this) {
            switchToMain()
        }

        initialiseWebView()
        setContentView(binding.root)

        onBackPressedDispatcher.addCallback(this , object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                if(binding.webViewSplash.onBackPressed()) finish()
            }
        })
    }

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_DOWN) {
            val v = currentFocus
            if (v is EditText) {
                val outRect = Rect()
                v.getGlobalVisibleRect(outRect)
                if (!outRect.contains(event.rawX.toInt(), event.rawY.toInt())) {
                    v.clearFocus()
                    val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0)
                }
            }
        }
        return super.dispatchTouchEvent(event)
    }

    override fun onResume() {
        super.onResume()
        binding.webViewSplash.onResume()
    }

    override fun onPause() {
        binding.webViewSplash.onPause()
        super.onPause()
    }

    override fun onDestroy() {
        binding.webViewSplash.onDestroy()
        (binding.viewmodel as CaloriesCalculatorSplashViewModel).destroy()
        super.onDestroy()
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)
        binding.webViewSplash.onActivityResult(requestCode, resultCode, intent)
    }

    private fun switchToMain() {
        val i = Intent(applicationContext, MainActivity::class.java)
        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NO_ANIMATION
        startActivity(i)
        finish()
    }

    private fun initialiseWebView() {
        binding.webViewSplash.setMixedContentAllowed(true)
        binding.webViewSplash.webChromeClient = object: WebChromeClient() {
            private var h = 0
            private var uiVisibility = 0
            private var d: View? = null
            private var a: CustomViewCallback? = null
            override fun getDefaultVideoPoster(): Bitmap? = if(d == null) null else BitmapFactory.decodeResource(resources, R.drawable.ic_baseline_video)
            override fun onHideCustomView() {
                (window.decorView as FrameLayout).removeView(d)
                d = null
                window.decorView.systemUiVisibility = uiVisibility
                requestedOrientation = h
                a!!.onCustomViewHidden()
                a = null
            }

            override fun onShowCustomView(view: View?, callback: CustomViewCallback?) {
                if(d != null) {
                    onHideCustomView()
                    return
                }
                d = view
                uiVisibility = window.decorView.systemUiVisibility
                h = requestedOrientation
                a = callback
                (window.decorView as FrameLayout).addView(d, FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT))
                WindowInsetsControllerCompat(window, window.decorView).let { controller ->
                    controller.hide(WindowInsetsCompat.Type.systemBars())
                    controller.systemBarsBehavior = WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
                }
            }
        }
        binding.webViewSplash.settings.userAgentString = binding.webViewSplash.settings.userAgentString.replace("; wv", "")
    }
}
