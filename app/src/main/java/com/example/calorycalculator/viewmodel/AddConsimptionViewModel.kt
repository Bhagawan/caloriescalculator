package com.example.calorycalculator.viewmodel

import com.example.calorycalculator.data.Consumption
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch

class AddConsumptionViewModel {

    var caloriesAmount = ""

    var reason = ""

    private val mutableButtonListener = MutableSharedFlow<Consumption?>()
    val buttonListener = mutableButtonListener.asSharedFlow()

    fun pressOk() {
        try {
            val c = Consumption(caloriesAmount.toInt(), reason)
            CoroutineScope(Dispatchers.Default).launch { mutableButtonListener.emit(c) }
        } catch (e: Exception) {
            pressCancel()
        }
    }

    fun pressCancel() = CoroutineScope(Dispatchers.Default).launch { mutableButtonListener.emit(null) }
}