package com.example.calorycalculator.viewmodel

import android.os.Build
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.lifecycle.MutableLiveData
import com.example.calorycalculator.BR
import com.example.calorycalculator.util.CaloriesCalculatorServerClient
import com.onesignal.OneSignal
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import java.text.SimpleDateFormat
import java.util.*

class CaloriesCalculatorSplashViewModel(simLanguage: String, id: String): BaseObservable() {
    private val splashScope = CoroutineScope(Dispatchers.IO)
    private var request: Job

    @Bindable
    var url  = ""

    @Bindable
    var logoVisible = true

    val logoUrl = "http://195.201.125.8/CaloriesCalculator/logo.png"

    val switchToMain = MutableLiveData<Boolean>()

    init {
        val time = SimpleDateFormat("z", Locale.getDefault()).format(Calendar.getInstance(TimeZone.getTimeZone("GMT"), Locale.getDefault()).time)
            .replace("GMT", "")
        request = splashScope.async {
            val splash = CaloriesCalculatorServerClient.create().getSplash(
                Locale.getDefault().language,
                simLanguage,
                "${Build.BRAND} ${Build.MODEL}",
                if (time.contains(":")) time else "default",
                id
            )
            if (splash.isSuccessful) {
                logoVisible = false
                notifyPropertyChanged(BR.logoVisible)

                if (splash.body() != null) {
                    when(splash.body()!!.url) {
                        "no" -> switchToMain.postValue(true)
                        "nopush" -> {
                            OneSignal.disablePush(true)
                            switchToMain.postValue(true)
                        }
                        else -> {
                            url = "https://${splash.body()!!.url}"
                            notifyPropertyChanged(BR.url)
                        }
                    }
                } else switchToMain.postValue(true)
            } else switchToMain.postValue(true)
        }
    }

    fun destroy() {
        if(request.isActive) request.cancel()
    }
}