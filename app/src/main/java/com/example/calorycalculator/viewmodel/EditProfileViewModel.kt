package com.example.calorycalculator.viewmodel

import android.widget.RadioGroup
import androidx.databinding.BaseObservable
import com.example.calorycalculator.R
import com.example.calorycalculator.data.Profile
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch

class EditProfileViewModel(): BaseObservable() {
    var age = ""
    var weight = ""
    var height = ""
    var regime = 0
    var gender = true

    constructor(profile: Profile) : this() {
        age = profile.age.toString()
        weight = profile.weight.toString()
        height = profile.height.toString()
        regime = profile.regime
        gender = profile.gender
    }

    private val mutableButtonListener = MutableSharedFlow<Profile?>()
    val buttonListener = mutableButtonListener.asSharedFlow()

    private val mutableErrorListener = MutableSharedFlow<String>()
    val errorListener = mutableErrorListener.asSharedFlow()

    fun confirm() {
        try {
            val ageF = age.toInt()
            val weightF = weight.toInt()
            val heightF = height.toInt()
            CoroutineScope(Dispatchers.Default).launch { mutableButtonListener.emit(Profile(ageF, weightF, heightF, gender, regime)) }
        } catch (e:Exception) {
            CoroutineScope(Dispatchers.Default).launch { mutableErrorListener.emit("Форма заполнена неверно") }
        }

    }

    fun cancel() = CoroutineScope(Dispatchers.Default).launch { mutableButtonListener.emit(null) }

    fun checkGender(radioGroup: RadioGroup, id: Int) {
        gender = id == R.id.radioButton_male
    }

    fun initGender() : Int {
        return if(gender) R.id.radioButton_male
        else R.id.radioButton_female
    }
}