package com.example.calorycalculator.viewmodel

import android.content.Context
import android.graphics.Color
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.example.calorycalculator.BR
import com.example.calorycalculator.data.Consumption
import com.example.calorycalculator.util.SavedPrefs
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch

class MainViewModel: BaseObservable() {
    private val scope = CoroutineScope(Dispatchers.Default)
    private var currGoal = 0f

    companion object {
        const val COMMAND_ADD_CONSUMPTION = 0
        const val COMMAND_EDIT_PROFILE = 1
        const val COMMAND_CLEAR = 2
    }

    @Bindable
    var todayCaloriesAmount = "0 кал"
    @Bindable
    var color = Color.WHITE

    private val mutableCommandFlow = MutableSharedFlow<Int>()
    val commandFlow = mutableCommandFlow.asSharedFlow()

    @Bindable
    val updateHistoryFlag = true
    @Bindable
    val updateGoalFlag = true

    fun getHistory(context: Context, flag: Boolean) : List<Consumption> {
        val history = SavedPrefs.getHistory(context)
        var total = 0
        for(case in history) total += case.calories
        todayCaloriesAmount = "$total кал"
        val c = (255 * todayCaloriesAmount.subSequence(0, todayCaloriesAmount.length - 4).toString().toInt() / currGoal).toInt().coerceAtMost(255)
        color = Color.rgb( 255,255 - c,255 - c)
        notifyPropertyChanged(BR.todayCaloriesAmount)
        notifyPropertyChanged(BR.color)
        return history
    }

    fun getGoal(context: Context, flag: Boolean) : String{
        val profile = SavedPrefs.getProfile(context)
        profile?.let {
            currGoal = if(profile.gender) 66 + (13.7f * profile.weight) + (5 * profile.height) - (6.8f * profile.age)
            else 655 + (9.6f * profile.weight) + (1.8f * profile.height) - (4.7f * profile.age)
            when (profile.regime) {
                0 -> currGoal -= 300
                2 -> currGoal += 1000
            }
            val c = (255 * todayCaloriesAmount.subSequence(0, todayCaloriesAmount.length - 4).toString().toInt() / currGoal).toInt().coerceAtMost(255)
            color = Color.rgb( 255,255 - c,255 - c)
            notifyPropertyChanged(BR.color)
            return "$currGoal кал/день"
        }
        return "0 кал/д"
    }

    fun pressAddConsumption() = scope.launch { mutableCommandFlow.emit(COMMAND_ADD_CONSUMPTION) }

    fun pressEditProfile() = scope.launch { mutableCommandFlow.emit(COMMAND_EDIT_PROFILE) }

    fun updateHistory() = notifyPropertyChanged(BR.updateHistoryFlag)

    fun updateGoal()  = notifyPropertyChanged(BR.updateGoalFlag)

    fun clear() = scope.launch { mutableCommandFlow.emit(COMMAND_CLEAR) }
}