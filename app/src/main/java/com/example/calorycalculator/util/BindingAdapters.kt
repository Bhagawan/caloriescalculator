package com.example.calorycalculator.util

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.calorycalculator.util.adapters.DynamicAdapter
import com.squareup.picasso.Picasso
import im.delight.android.webview.AdvancedWebView


@BindingAdapter("imageUrl")
fun imageUrl(view: ImageView, url: String) {
    Picasso.get().load(url).into(view)
}

@BindingAdapter("url")
fun setUrl(v: AdvancedWebView, url: String) {
    if(url.isNotEmpty()) v.loadUrl(url)
}

@Suppress("UNCHECKED_CAST")
@BindingAdapter("recyclerData")
fun <T> setData(recycler: RecyclerView, data: T?) {
    if(recycler.adapter != null && data != null){
        if(recycler.adapter is DynamicAdapter<*>)(recycler.adapter as DynamicAdapter<T>).setData(data)
    }
}