package com.example.calorycalculator.util

import android.content.Context
import com.example.calorycalculator.data.Consumption
import com.example.calorycalculator.data.Profile
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.text.SimpleDateFormat
import java.util.*

class SavedPrefs {
    companion object {
        fun saveProfile(context: Context, profile : Profile) {
            val shP = context.getSharedPreferences("AppData", Context.MODE_PRIVATE)
            shP.edit().putString("profile", Gson().toJson(profile)).apply()
        }

        fun getProfile(context: Context) : Profile? {
            val shP = context.getSharedPreferences("AppData", Context.MODE_PRIVATE)
            return Gson().fromJson(shP.getString("profile", ""), Profile::class.java)
        }

        fun addConsumption(context: Context, consumption: Consumption) {
            val shP = context.getSharedPreferences("AppData", Context.MODE_PRIVATE)
            val lastDate = shP.getString("lastDate", "")
            val df = SimpleDateFormat("yyyy MM dd", Locale.getDefault())
            val g = Gson()

            val newHistory = if(lastDate != df.format(Date())) {
                shP.edit().putString("lastDate", df.format(Date())).apply()
                listOf(consumption)
            } else {
                val oldHistory = g.fromJson(shP.getString("dayHistory", "[]"), object: TypeToken<List<Consumption>>() {}.type)?: emptyList<Consumption>()
                oldHistory.plus(consumption)
            }

            shP.edit().putString("dayHistory", g.toJson(newHistory)).apply()
        }

        fun getHistory(context: Context) : List<Consumption> {
            val shP = context.getSharedPreferences("AppData", Context.MODE_PRIVATE)
            val lastDate = shP.getString("lastDate", "")
            val df = SimpleDateFormat("yyyy MM dd", Locale.getDefault())

            return if(lastDate != df.format(Date())) {
                clearHistory(context)
                emptyList()
            } else Gson().fromJson(shP.getString("dayHistory", "[]"), object: TypeToken<List<Consumption>>() {}.type)?: emptyList()
        }

        fun clearHistory(context: Context) {
            val shP = context.getSharedPreferences("AppData", Context.MODE_PRIVATE)
            shP.edit().putString("dayHistory", Gson().toJson(emptyList<Consumption>())).apply()
        }

        fun getId(context: Context) : String {
            val shP = context.getSharedPreferences("AppData", Context.MODE_PRIVATE)
            var id = shP.getString("id", "default") ?: "default"
            if(id == "default") {
                id = UUID.randomUUID().toString()
                shP.edit().putString("id", id).apply()
            }
            return id
        }
    }
}