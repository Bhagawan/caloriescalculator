package com.example.calorycalculator.util.adapters

interface DynamicAdapter<T> {
    fun setData(data: T)
}