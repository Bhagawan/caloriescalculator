package com.example.calorycalculator

import android.graphics.Rect
import android.os.Bundle
import android.view.MotionEvent
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.example.calorycalculator.databinding.ActivityProfileBinding
import com.example.calorycalculator.util.SavedPrefs
import com.example.calorycalculator.viewmodel.EditProfileViewModel
import kotlinx.coroutines.launch

class ProfileActivity : AppCompatActivity() {
    private lateinit var binding: ActivityProfileBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityProfileBinding.inflate(layoutInflater)
        val profile = SavedPrefs.getProfile(this)
        binding.viewModel = if(profile != null) EditProfileViewModel(profile) else EditProfileViewModel()

        lifecycleScope.launch {
            binding.viewModel?.buttonListener?.collect {
                it?.let { SavedPrefs.saveProfile(this@ProfileActivity, it) }
                finish()
            }
        }
        lifecycleScope.launch {
            binding.viewModel?.errorListener?.collect {
                Toast.makeText(this@ProfileActivity, it, Toast.LENGTH_SHORT).show()
            }
        }
        setContentView(binding.root)
    }

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_DOWN) {
            val v = currentFocus
            if (v is EditText) {
                val outRect = Rect()
                v.getGlobalVisibleRect(outRect)
                if (!outRect.contains(event.rawX.toInt(), event.rawY.toInt())) {
                    v.clearFocus()
                    val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0)
                }
            }
        }
        return super.dispatchTouchEvent(event)
    }
}