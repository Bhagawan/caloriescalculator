package com.example.calorycalculator.data

import androidx.annotation.Keep

@Keep
data class Profile(val age: Int, val weight : Int, val height : Int, val gender : Boolean, val regime : Int)