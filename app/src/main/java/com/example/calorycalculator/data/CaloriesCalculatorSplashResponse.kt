package com.example.calorycalculator.data

import androidx.annotation.Keep

@Keep
data class CaloriesCalculatorSplashResponse(val url : String)