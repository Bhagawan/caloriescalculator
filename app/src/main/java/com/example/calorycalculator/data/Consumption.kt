package com.example.calorycalculator.data

data class Consumption(val calories : Int, val note : String)
